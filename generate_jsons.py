import os, pdb, json
import pandas as pd
import numpy as np
import shutil

"""
It creates this all_languages.csv file with all the languages (but it is not well formatted on the web IDE)
"""
keywords_df = pd.read_csv('all_languages.csv')
print(f"we have {len(keywords_df)}")

print('THIS WILL GENERATE NEW JSON FILES!')
pdb.set_trace()

j_de = {}
j_en = {}
j_es = {}
j_it = {}
j_fr = {}

for index, k in enumerate(keywords_df['keywords']):
    j_de[k] = keywords_df['de'][index]
    j_en[k] = keywords_df['en'][index]
    j_es[k] = keywords_df['es'][index]
    j_it[k] = keywords_df['it'][index]
    j_fr[k] = keywords_df['fr'][index]

with open(os.path.join('i18n', 'de.json'), 'w') as user_file:
    json.dump(j_de, user_file, indent=2)
with open(os.path.join('i18n', 'en.json'), 'w') as user_file:
    json.dump(j_en, user_file, indent=2)
with open(os.path.join('i18n', 'es.json'), 'w') as user_file:
    json.dump(j_es, user_file, indent=2)
with open(os.path.join('i18n', 'it.json'), 'w') as user_file:
    json.dump(j_it, user_file, indent=2)
with open(os.path.join('i18n', 'fr.json'), 'w') as user_file:
    json.dump(j_fr, user_file, indent=2)
